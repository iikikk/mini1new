+++
title = "My hobbies"
date = 2024-01-30
description = "What I interested"
[taxonomies]
tags = ["hobby"]
+++

I love nature, the mountains and the sea give me a sense of inner peace. I enjoy hiking in my free time, and it gives me a great sense of satisfaction to climb to the top of a mountain and see the scenery at the bottom of my feet.


![Bamford Edge](../mountain.jpg)

Musicals and films are also my hobbies. I love all kinds of films, except for the horror genre. I am a fan of Greta Gerwig and my favourite film is Frances Ha starring in her. I like a lot of West End and Broadway musicals, Hamilton, Les Misérables, Spring Awakening are some of the ones I really like.

![Hamilton](../hamilton.jpg)

[^zolawebsite]: Website: [zola](https://getzola.org), GitHub: [getzola/zola](https://github.com/getzola/zola)