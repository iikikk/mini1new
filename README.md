# IDS721 Individual Project 1

## Gitlab repository:
https://gitlab.com/iikikk/mini1new/-/tree/main

## Demo
### Vercel:
https://mini1new4.vercel.app/
### Gitlab:
https://mini1new-iikikk-978f0c6de80f36e4a713455ccb511b3624938568cff9cfb.gitlab.io/

## Demo video: (I have uploaded it in my root directory)
https://gitlab.com/iikikk/mini1new/-/blob/main/individual1.mkv


## Zola setup:

1. Install zola: sudo apt install zola
2. Create zola project: zola init myblog
3. Run zola locally: zola serve
4. Build the website: zola build

## Website Functionality

### Templates:

I used the following zola tamplate:
https://github.com/oltdaniel/dose
It consists of three main pages:

- base.html - fundamental structures found in all HTML and global CSS
- index.html - home page template
- page.html – individual page template

### Pages
There are three pages in the site for now:
1. Home page: main page which could redirect to “About me” page and “My hobbies” page.
2. About me page: Page that gives a brief introduction about me,
3. My hobbies page: Page that gives a brief introduction about my hobbies.


## GitLab Workflow:
1. Create .gitlab-ci.yml in the root directory of the repository:
```javascript
  stages:
    - deploy

  default:
    image: debian:stable-slim
    tags:
      - docker

  variables:
    # The runner will be able to pull your Zola theme when the strategy is
    # set to "recursive".
    GIT_SUBMODULE_STRATEGY: "recursive"

    # If you don't set a version here, your site will be built with the latest
    # version of Zola available in GitHub releases.
    # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
    ZOLA_VERSION:
      description: "The version of Zola used to build the site."
      value: ""

  pages:
    stage: deploy
    script:
      - |
        apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
        if [ $ZOLA_VERSION ]; then
          zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
          if ! wget --quiet --spider $zola_url; then
            echo "A Zola release with the specified version could not be found.";
            exit 1;
          fi
        else
          github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
          zola_url=$(
            wget --output-document - $github_api_url |
            grep "browser_download_url.*linux-gnu.tar.gz" |
            cut --delimiter : --fields 2,3 |
            tr --delete "\" "
          )
        fi
        wget $zola_url
        tar -xzf *.tar.gz
        ./zola build

    artifacts:
      paths:
        # This is the directory whose contents will be deployed to the GitLab Pages
        # server.
        # GitLab Pages expects a directory with this name by default.
        - public

    rules:
      # This rule makes it so that your website is published and updated only when
      # you push to the default branch of your repository (e.g. "master" or "main").
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
2. Get the URL at Gitlab -> Deploy -> Pages:
https://mini1new-iikikk-978f0c6de80f36e4a713455ccb511b3624938568cff9cfb.gitlab.io/


## Hosting and Deployment in Vercel:

1. Create a new Vercel account and linked to Gitlab account.
2. Add new project and import the corresponding Gitlab repository you would like to linked to Vercel.
3. Choose framework as Zola and deploy.
4. The following is my url in Vercel: https://mini1new4.vercel.app/

## screenshot

### Deploy on GitLab

 ![Alt text](image.png)
 ![Alt text](image-1.png)

### Deploy on Vercel

 ![Alt text](image-2.png)

### Pages:

- Home page:

![Alt text](image-3.png)

- About me page:

 ![Alt text](image-4.png)

- My hobbies page:

![Alt text](image-5.png)
 
